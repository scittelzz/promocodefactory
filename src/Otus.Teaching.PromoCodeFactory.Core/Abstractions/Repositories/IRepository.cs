﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);
        void AddEmployee(T _employee);
        void UpdateEmployee(Guid id, string email, string firstName, string lastName, string roles, int appliedPromocodesCount);
        void DeleteEmployee(Guid id);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]/[Action]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        /// <summary>
        /// Добавление сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public void AddEmployee(string email, string firstName, string lastName, string roles, int appliedPromocodesCount)
        {
            Role _role = new Role()
            {
                Name = roles,
                Description = ""
            };
            Employee _employee = new()
            {
                Id = Guid.NewGuid(),
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                Roles = new List<Role>()
                {
                    _role
                },
                AppliedPromocodesCount = appliedPromocodesCount
            };
            _employeeRepository.AddEmployee(_employee);
        }
        /// <summary>
        /// обновление данных сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPost("{id:guid}")]
        public void UpdateEmployee(Guid id, string email, string firstName, string lastName, string roles, int appliedPromocodesCount)
        {    
            _employeeRepository.UpdateEmployee(id, email, firstName, lastName, roles, appliedPromocodesCount);
        }
        /// <summary>
        /// Удаление данных сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPost("{id:guid}")]
        public void DeleteEmployee(Guid id)
        {
            _employeeRepository.DeleteEmployee(id);
        }
    }
}
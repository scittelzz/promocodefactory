﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        public void AddEmployee(T _employee)
        {
            var allItems = Data.ToList();
            allItems.Add(_employee);
            Data = allItems.AsEnumerable();
        }
        public void UpdateEmployee(Guid id, string email, string firstName, string lastName, string roles, int appliedPromocodesCount)
        {
            Data.Cast<Employee>().Where(i => i.Id == id).ToList().ForEach(el =>
            {
                el.Email = string.IsNullOrEmpty(email) ? el.Email : email;
                el.FirstName = string.IsNullOrEmpty(firstName) ? el.FirstName : firstName;
                el.LastName = string.IsNullOrEmpty(lastName) ? el.LastName : lastName;
                el.AppliedPromocodesCount = appliedPromocodesCount;
                el.Roles.Add(new Role
                {
                    Name = roles,
                    Description = ""
                });
            });
        }
        public void DeleteEmployee(Guid id)
        {
            Data = Data.Where(item => item.Id != id);
        }
    }
}